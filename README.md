# Physics 231 Final WIP

## Abstract

Most mathematical constructions may be represented as graphs, and subsequently as categories; in isolation these categories aren't typically of much use, at least when compared to the insights that the functors between categories yield.

For my final project in Physics 231: General Physics III I am exploring, and attempting to define, the relationship between two categories that have interested mathematicians and physicists for decades: the category of factor trees and Feynman Diagrams, respectively.

## Category of factor trees

A factor tree consists of a root, the number being factored, and sub nodes of factors; the prime factors are represented as leaves. This situation can be represented as a symmetric monoidal category. Let's breake down what each of those terms mean.

- Category: A category is a colection of objects and morphisms; categories are a type of psuedograph, thinking about categories as directed graphs is helpful.

- Monoidal: A monoidal category, is a category that has both a binary operator that is assosiative and an identity element.

- Symmetric: Our category is equiped with a braid $`b_{xy} :: X \otimes Y \rarr Y \otimes X \equiv b_{yx} :: Y \otimes X \rarr X \otimes Y \rArr X \otimes Y \backsimeq Y \otimes X`$

We're secretly dealing with GL(1,N), which means that our tensor product is just our friendly neighborhood multiplication.

Categories all the way down. If a single factor tree can be represented as a category, then we can consider the category of all categories representing factor trees.

If our category is discrete then we only require one morphism, the identity, which sends an object X to itself and is denoted $`id_X`$. In a category that is not discrete we need to adress how our objects compose. In the category of factor tress -for now- we imagine each object is a binary graph. The root is some natural number, and the children are its factors. For a given number, there may be many such trees. If the leaves are not all prime factors, we can glue another tree to one of the non-prime children, recursively, until all the leaves are prime. This will be our notion of object composition.

The identity morphism of any graph with leaves $`n_1, n_2, ... n_i`$ is the graph with the root $`n_i`$ and the set of children $`{1,n_i}`$.

It will help to consider the point line dual of our newly created category. The point line dual is the category in which each object becomes a morphism and each morphism becomes an object. Doing so we can now consider our graph as a spin network, where each wire represents a factor tree, and each point represnts the composition with another tree. A wire is complete when the final component goes to the identity object whis is isomorphic to all tress with no compositions other than $`id`$.

id serves as an internal hom on our category of factor trees

Briefly returning to composition, our definition is incomplete, because we require information about our object to choose our morphisms. In category theory, that information is hidden. So we'll have to slightly modify our definition.

Currying adresses the issue of composing graphs with more than one non-prime leaf.


## Feynman Diagrams

Feynman diagrams, are graphs, which may be represented as braided monoidal categories.

## Functor

A functor, in a general sense, is a mapping between categories.

## Spectral Representation

